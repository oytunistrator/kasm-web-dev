#!/bin/bash
set -ex

add-apt-repository -y ppa:ondrej/php

apt-get update
apt-get install -y php nodejs npm php-mysql php-curl php-xmlrpc php-intl php-gd phpunit php-yaml php-xml php-zip vim nano php-symfony mysql-client php-mysql php-memcache 
