#!/bin/bash
set -ex

wget -q https://getcomposer.org/download/latest-stable/composer.phar -O /usr/bin/composer
chmod a+x /usr/bin/composer
